---
- hosts: all
  become: yes
  become_method: sudo
  remote_user: root

  vars:
    user: "fdroid"
    userhome: "/home/{{ user }}"
    jobname: "run_makebuildserver"
    screenrc: "{{ userhome }}/{{ jobname }}.screenrc"

  tasks:

    - name: "assert: {{ ansible_memtotal_mb }}MB RAM and {{ ansible_processor_vcpus }} CPU are sufficient"
      assert:
        that:
          - ansible_memtotal_mb >= 4963 # Vagrantfile calls this 5120
          - ansible_processor_vcpus > 1

    - name: "apt: install debian packages for ansible secure apt setup"
      apt:
        name: "{{ item }}"
        install_recommends: no
        update_cache: yes
      with_items:
        - apt-transport-https
        - apt-transport-tor
        - debian-archive-keyring
        - gnupg
        - tor
        - python-apt

    - name: "default to libvirt if 'vm_provider' is not set"
      set_fact:
        vm_provider: "libvirt"
      when: vm_provider is undefined

    - name: "systemd: make sure tor is running"
      systemd:
        name: tor
        enabled: yes
        state: started

    - name: "apt_repository: sgvtcaew4bxjd7ln.onion"
      apt_repository:
        repo: 'deb tor+http://sgvtcaew4bxjd7ln.onion/debian-security {{ ansible_distribution_release }}/updates main'
        state: present
        filename: 0.sgvtcaew4bxjd7ln.onion
        update_cache: no

    - name: "apt_repository: vwakviie2ienjx6t.onion"
      copy:
        content: |
          deb tor+http://vwakviie2ienjx6t.onion/debian/ {{ ansible_distribution_release }} main
          #deb-src tor+http://vwakviie2ienjx6t.onion/debian/ {{ ansible_distribution_release }} main
          deb tor+http://vwakviie2ienjx6t.onion/debian/ {{ ansible_distribution_release }}-updates main
          #deb-src tor+http://vwakviie2ienjx6t.onion/debian/ {{ ansible_distribution_release }}-updates main
          deb tor+http://vwakviie2ienjx6t.onion/debian/ {{ ansible_distribution_release }}-backports main
          #deb-src tor+http://vwakviie2ienjx6t.onion/debian/ {{ ansible_distribution_release }}-backports main
        dest: /etc/apt/sources.list.d/0.vwakviie2ienjx6t.onion.list
        mode: 0644
        owner: root
        group: root

    - name: "apt_repository: deb.debian.org"
      copy:
        content: |
          deb https://deb.debian.org/debian/ {{ ansible_distribution_release }} main
          #deb-src https://deb.debian.org/debian/ {{ ansible_distribution_release }} main
          deb https://deb.debian.org/debian/ {{ ansible_distribution_release }}-updates main
          #deb-src https://deb.debian.org/debian/ {{ ansible_distribution_release }}-updates main
          deb https://deb.debian.org/debian-security/ {{ ansible_distribution_release }}/updates main
          #deb-src https://deb.debian.org/debian-security/ {{ ansible_distribution_release }}/updates main
          deb https://deb.debian.org/debian/ {{ ansible_distribution_release }}-backports main
          #deb-src https://deb.debian.org/debian/ {{ ansible_distribution_release }}-backports main
        dest:  /etc/apt/sources.list.d/9.deb.debian.org.list
        mode: 0644
        owner: root
        group: root

    # VirtualBox is not in Debian/buster
    - name: "apk_key: Add Lucas Nussbaum <lucas@debian.org> key"
      apt_key:
        url: https://db.debian.org/fetchkey.cgi?fingerprint=FEDEC1CB337BCF509F43C2243914B532F4DFBE99
        state: present
      when: ansible_distribution == 'Debian' and ansible_distribution_release == 'buster' and vm_provider == 'virtualbox'

    - name: "apt_repository: Add lucas@debian.org's Virtualbox backport for Debian 10 apt repo"
      apt_repository:
        repo: 'deb tor+https://people.debian.org/~lucas/virtualbox-buster/ ./'
        update_cache: no
        state: present
      when: ansible_distribution == 'Debian' and ansible_distribution_release == 'buster' and vm_provider == 'virtualbox'

    - name: "apt_repository: security.debian.org"
      apt_repository:
        repo: 'deb http://security.debian.org/debian-security {{ ansible_distribution_release }}/updates main'
        filename: 9.security.debian.org

    - name: "copy: clear /etc/apt/sources.list"
      copy:
        content: ""
        dest: "/etc/apt/sources.list"
        mode: 0644
        owner: root
        group: root

    - name: "wait for tor daemon to start proxying"
      wait_for:
        host: localhost
        port: 9050

    - name: "apt: dist-upgrade"
      apt:
        update_cache: yes
        upgrade: dist

    - name: "include_role: install vagrant-libvirt and kvm (amd64)"
      include_role: name=ansible-debian-install-vagrant-libvirt
      when: vm_provider == 'libvirt'

    - name: "include_role: install fdroidserver dependencies"
      include_role: name=ansible-role-install-fdroidserver-dependencies
      vars:
        apt_update_cache: true

    - name: "apt: install debian packages"
      apt:
        name: "{{ item }}"
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:

        # automatically install security updates that don't require reboots
        - unattended-upgrades

        # handy utilities
        - bash-completion
        - curl
        - emacs-nox
        - figlet
        - htop
        - iotop
        - less
        - ncdu
        - vim
        - wget

        # bootstrap script dependencies
        - ca-certificates
        - locales
        - openssh-server
        - screen
        - sudo
        - dbus # (timedatectl (systemd) dependency)

    - name: "apt: install VirtualBox and dependencies"
      apt:
        name: "{{ item }}"
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:
        - linux-headers-amd64
        - vagrant
        - virtualbox
        - virtualbox-dkms
      when: vm_provider == 'virtualbox'

    - name: "apt: remove any leftover libvirt dependencies"
      apt:
        autoclean: yes
        autoremove: yes
        state: absent
        purge: yes
        name:
          - libvirt-clients
          - libvirt-daemon
          - libvirt-daemon-system
          - ruby-fog-libvirt
          - ruby-libvirt
          - vagrant-libvirt
      when: vm_provider == 'virtualbox'

    - name: "systemd: make sure virtualbox is running"
      systemd:
        state: started
        name: virtualbox
      when: vm_provider == 'virtualbox'

    - name : "modprobe: vboxpci"
      modprobe:
        name: vboxpci
      when: vm_provider == 'virtualbox'
    - name : "modprobe: vboxnetadp"
      modprobe:
        name: vboxnetadp
      when: vm_provider == 'virtualbox'
    - name : "modprobe: vboxnetflt"
      modprobe:
        name: vboxnetflt
      when: vm_provider == 'virtualbox'

    # just in case https://phoenhex.re/2018-03-25/not-a-vagrant-bug
    - name: "copy: disable symlinks in shared folders"
      copy:
        content: |
          # https://phoenhex.re/2018-03-25/not-a-vagrant-bug
          export VAGRANT_DISABLE_VBOXSYMLINKCREATE=1
        dest: /etc/profile.d/vagrant_disable_symlinks_in_shared_folders.sh
        mode: 0644
        owner: root
        group: root

    - name: "copy: script for updating with apt"
      copy:
        mode: 0700
        content: |
          #!/bin/sh

          set -x
          apt-get update
          apt-get -y dist-upgrade --download-only

          set -e
          apt-get -y upgrade
          apt-get dist-upgrade
          apt-get autoremove --purge
          apt-get clean
        dest: /root/update-all

    - name: "timezone: set system to Etc/UTC"
      timezone:
        name: Etc/UTC

    - name: "file: create symbolic link to enable all locales"
      file:
        src: "/usr/share/i18n/SUPPORTED"
        dest: "/etc/locale.gen"
        state: link
        force: yes

    - name: "local_gen: find all locales"
      shell: "grep -Eo '^ *[^#][^ ]+\\.UTF-8' /etc/locale.gen"
      register: locales_grep

    - name: "locale_gen: generate all locales"
      locale_gen: "name={{ item }} state=present"
      with_items: "{{ locales_grep.stdout_lines }}"

    - name: 'lineinfile: set default system locale to en_US.UTF-8'
      lineinfile:
        dest: "/etc/default/locale"
        line: "LANG=en_US.UTF-8"

    - name: "shell: set motd"
      shell: |
        echo > /etc/motd
        figlet buildserver >> /etc/motd
        printf '\ncreated with https://gitlab.com/fdroid/fdroid-bootstrap-buildserver\n\n' >> /etc/motd

    - name: 'include_role: ensure kvm nesting is enabled'
      include_role: name=ansible-debian-enable-kvm-nesting
      when: vm_provider == 'libvirt'

    - name: "user: add {{ user }} to libvirt groups"
      user: "name={{ user }} groups=libvirt,libvirt-qemu shell=/bin/bash"
      when: vm_provider == 'libvirt'

    - name: "user: add '{{ user }}'"
      user:
        name: "{{ user }}"
        comment: F-Droid User
        shell: /bin/bash

    - name: "file: make ~{{ user }}/.ssh"
      file:
        path: "{{ userhome }}/.ssh"
        state: directory
        mode: 0755
        owner: root
        group: root

    - name: "file: make /root/.ssh"
      file:
        path: /root/.ssh
        state: directory
        mode: 0700
        owner: root
        group: root

    # TODO: remove this workaround once makebuildserver does not need it anymore
    - name: "copy: grant rights to {{ user }} for changing access permissions to vm image"
      copy:
        content: |
          %{{ user }} ALL=(ALL) NOPASSWD: /bin/chmod -R a+rX /var/lib/libvirt/images
        dest: "/etc/sudoers.d/{{ user }}-libvirt-images"
        mode: 0644
        owner: root
        group: root
      when: vm_provider == 'libvirt'

    - name: "copy: grant rights to {{ user }} for setting up NFS"
      copy:
        content: |
          # https://github.com/hashicorp/vagrant/blob/b16794c/website/source/docs/synced-folders/nfs.html.md#root-privilege-requirement
          Cmnd_Alias EXPORTS_CHOWN = /bin/chown 0\:0 /tmp/vagrant[a-z0-9-]*
          Cmnd_Alias EXPORTS_MV = /bin/mv -f /tmp/vagrant[a-z0-9-]* /etc/exports
          Cmnd_Alias NFSD_SYSV_CHECK = /etc/init.d/nfs-kernel-server status
          Cmnd_Alias NFSD_SYSV_START = /etc/init.d/nfs-kernel-server start
          Cmnd_Alias NFSD_CHECK = /bin/systemctl status --no-pager nfs-server.service
          Cmnd_Alias NFSD_START = /bin/systemctl start nfs-server.service
          Cmnd_Alias NFSD_APPLY = /usr/sbin/exportfs -ar
          %{{ user }} ALL=(root) NOPASSWD: EXPORTS_CHOWN, EXPORTS_MV, NFSD_SYSV_CHECK, NFSD_SYSV_START, NFSD_CHECK, NFSD_START, NFSD_APPLY
        dest: "/etc/sudoers.d/{{ user }}-vagrant-nfs"
        mode: 0644
        owner: root
        group: root
      when: vm_provider == 'libvirt'

    - name: "git: clone fdroidserver"
      git:
        repo: 'https://gitlab.com/fdroid/fdroidserver.git'
        dest: '{{ userhome }}/fdroidserver'
        version: master
      become_user: "{{ user }}"
    - name: 'lineinfile: add fdroid server to users path'
      lineinfile:
        dest: "{{ userhome }}/.profile"
        line: "export PATH=$PATH:{{ userhome }}/fdroidserver"
      become_user: "{{ user }}"

    - name: "copy: configure makebuildserver.config.py"
      copy:
        content: |
          memory = 4096
          cpus = 2
          vm_provider = "{{ vm_provider }}"
          apt_package_cache = True
        dest: "{{ userhome }}/fdroidserver/makebuildserver.config.py"
        mode: 0644
        owner: "{{ user }}"
        group: "{{ user }}"

    - name: "git: clone fdroiddata"
      git:
        repo: 'https://gitlab.com/fdroid/fdroiddata.git'
        dest: "{{ userhome }}/fdroiddata"
        version: master
      become_user: "{{ user }}"

    - name: "copy: screenrc for {{ user }} for running scripts"
      copy:
        content: |
          utf8 on on
          defutf8 on
          log on
          logfile {{ userhome }}/{{ jobname }}.log
          sessionname {{ jobname }}
          shelltitle {{ jobname }}
        mode: 0640
        owner: root
        group: "{{ user }}"
        dest: "{{ screenrc }}"

    - name: "copy: script for running makebuildserver in screen"
      copy:
        content: |
          #!/bin/sh
          cd {{ userhome }}/fdroidserver
          ./makebuildserver --verbose
        mode: 0750
        owner: root
        group: "{{ user }}"
        dest: "{{ userhome }}/{{ jobname }}"

    - name: "shell: run ./makebuildserver if not currently running"
      shell: |
        set -e
        jobname="{{ jobname }}"
        rm -f {{ userhome }}/${jobname}.log
        if [ -e /run/screen/S-{{ user }}/* ]; then
            exit 0
        fi
        screen -d -m -L -c {{ screenrc }} {{ userhome }}/${jobname}
      args:
        executable: /bin/sh
      become_user: "{{ user }}"
